package ufc.br.usodroga;

import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;


import ufc.br.usodroga.dialogos.DialogNickName;

import static java.lang.Thread.*;

public class FullScreenActivity extends AppCompatActivity implements DialogNickName.NoticeDialogListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_primary);
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new DialogNickName().show(getSupportFragmentManager(), "");
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {

    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }
}
